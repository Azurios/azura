// #![allow(unused_imports)]
#[macro_use]
extern crate lazy_static;
mod commands;

use commands::{emote::*, fortune::*, roll::*, tarot::*, waifu::*};

use gitlab::api::{self, projects, Query};
use gitlab::Gitlab;

use serenity::{
    async_trait,
    client::bridge::gateway::ShardManager,
    framework::standard::{
        help_commands,
        macros::{check, command, group, help, hook},
        Args, CommandGroup, CommandOptions, CommandResult, DispatchError, HelpOptions, Reason,
        StandardFramework,
    },
    http::Http,
    model::{
        channel::Message,
        gateway::Ready,
        id::UserId,
        interactions::{
            ApplicationCommand, Interaction, InteractionApplicationCommandCallbackDataFlags,
            InteractionResponseType,
        },
    },
};

use std::io::prelude::*;
use std::io::Write;
use std::{collections::HashSet, sync::Arc};

use serenity::prelude::*;
use tokio::sync::Mutex;
const PRIVSERVER: serenity::model::id::GuildId = serenity::model::id::GuildId(176380431464267776);
const ETHSERVER: serenity::model::id::GuildId = serenity::model::id::GuildId(747752542741725244);

// A container type is created for inserting into the Client's `data`, which
// allows for data to be accessible across all events and framework commands, or anywhere else that has a copy of the `data` Arc.
struct ShardManagerContainer;

impl TypeMapKey for ShardManagerContainer {
    type Value = Arc<Mutex<ShardManager>>;
}

struct Handler;

#[async_trait]
impl EventHandler for Handler {
    async fn interaction_create(&self, ctx: Context, interaction: Interaction) {
        interaction
            .create_interaction_response(&ctx.http, |response| {
                response
                    .kind(InteractionResponseType::ChannelMessageWithSource)
                    .interaction_response_data(|message| {
                        message
                            .content("Received event!")
                            .flags(InteractionApplicationCommandCallbackDataFlags::EPHEMERAL)
                    })
            })
            .await
            .expect("got an event, but sth bad happened");
    }

    async fn ready(&self, ctx: Context, ready: Ready) {
        println!("{} is connected!", ready.user.name);
        ctx.shard
            .set_activity(Some(serenity::model::prelude::Activity::listening(
                "tilde, not dash",
            )));

        // build initial emote cache
        println!("Building emote cache...");
        let mut map = EMOTEMAP.write().await;
        let emotes = PRIVSERVER.emojis(&ctx).await.unwrap();
        for emote in emotes {
            let name = emote.name.clone();
            map.insert(name, emote);
        }
        let numemo = map.len();
        println!("Cache built, holding {} emotes", numemo);
    }
}

#[hook]
async fn before(_ctx: &Context, msg: &Message, command_name: &str) -> bool {
    println!(
        "Got command '{}' by user '{}'",
        command_name, msg.author.name
    );
    true // if `before` returns false, command processing doesn't happen.
}

#[hook]
async fn after(_ctx: &Context, _msg: &Message, command_name: &str, command_result: CommandResult) {
    match command_result {
        Ok(()) => {
            println!("Processed command '{}'", command_name);
            //msg.react(&ctx.http, '🌹').await.ok();
        }
        Err(why) => {
            println!("Command '{}' returned error {:?}", command_name, why);
            //msg.react(&ctx.http, '🥀').await.ok();
        }
    }
}

#[help]
#[individual_command_tip = "Hi there! I'm Azura. Learn more about commands with `~help <command>`\n"]
#[no_help_available_text = "I don't know this command, are you sure it exists?"]
// Define the maximum Levenshtein-distance between a searched command-name
// and commands. If the distance is lower than or equal the set distance,
// it will be displayed as a suggestion.
// Setting the distance to 0 will disable suggestions.
#[max_levenshtein_distance(1)]
// When you use sub-groups, Serenity will use the `indention_prefix` to indicate
// how deeply an item is indented.
// The default value is "-", it will be changed to "+".
#[indention_prefix = "+"]
// On another note, you can set up the help-menu-filter-behaviour.
// Here are all possible settings shown on all possible options.
// First case is if a user lacks permissions for a command, we can hide the command.
#[lacking_permissions = "Hide"]
// If the user is nothing but lacking a certain role, we just display it hence our variant is `Nothing`.
#[lacking_role = "Nothing"]
// The last `enum`-variant is `Strike`, which ~~strikes~~ a command.
#[wrong_channel = "Strike"]
async fn help(
    context: &Context,
    msg: &Message,
    args: Args,
    help_options: &'static HelpOptions,
    groups: &[&'static CommandGroup],
    owners: HashSet<UserId>,
) -> CommandResult {
    help_commands::with_embeds(context, msg, args, help_options, groups, owners).await;
    Ok(())
}

#[tokio::main]
async fn main() {
    let token = std::fs::read_to_string(".token").expect("error reading token");
    let token = token.trim();
    let prefix = std::fs::read_to_string(".prefix").expect("error reading prefix");
    let prefix = prefix.trim();
    println!("{}", prefix);

    let http = Http::new_with_token(&token);

    // We will fetch your bot's owners and id
    let (owners, _bot_id) = match http.get_current_application_info().await {
        Ok(info) => {
            let mut owners = HashSet::new();
            owners.insert(info.owner.id);
            (owners, info.id)
        }
        Err(why) => panic!("Could not access application info: {:?}", why),
    };

    let framework = StandardFramework::new()
        .help(&HELP)
        .configure(|c| {
            c
                //sets prefix
                .prefix(&prefix)
                //sets owners :D
                .owners(owners)
        })
        .normal_message(normal_message)
        .unrecognised_command(unknown_command)
        .before(before)
        .after(after)
        .on_dispatch_error(dispatcherr)
        .group(&GENERAL_GROUP)
        .group(&META_GROUP)
        .group(&MAINTENANCE_GROUP);

    let mut client = Client::builder(&token)
        .event_handler(Handler)
        .framework(framework)
        .application_id(823237524386218016)
        .await
        .expect("Err creating client");

    if let Err(why) = client.start().await {
        println!("Client error: {:?}", why);
    }
}

#[hook]
async fn dispatcherr(ctx: &Context, msg: &Message, err: DispatchError) {
    match err {
        DispatchError::NotEnoughArguments { min, given } => {
            msg.channel_id.say(&ctx, format!("Sorry, but I can't handle the command this way. You gave me {} arguments, while I need at least {}. Maybe try again?", given, min)).await.unwrap();
        }
        DispatchError::TooManyArguments { max, given } => {
            msg.channel_id.say(&ctx, format!("Sorry, but I can't handle the command this way. You gave me {} arguments, while I need at most {}. Maybe try again?", given, max)).await.unwrap();
        }
        DispatchError::OnlyForOwners => {
            msg.channel_id
                .say(
                    &ctx,
                    format!(
                        "I'm sorry, {}. I'm afraid I can't let you do that.",
                        msg.author
                    ),
                )
                .await
                .unwrap();
        }
        _ => {
            println!("Something went wrong - Perhaps a failed check?");
        }
    }
}
#[hook]
async fn normal_message(_ctx: &Context, _msg: &Message) {
    // println!("Message is not a command '{}'", msg.content);
}

#[hook]
async fn unknown_command(_ctx: &Context, _msg: &Message, unknown_command_name: &str) {
    //let say_content = format!("Sorry, I don't know the command '{}' (yet)", unknown_command_name);
    println!("Could not find command named '{}'", unknown_command_name);
    //msg.channel_id.say(&ctx.http, say_content).await;
}

#[group]
#[commands(roll, emote, react, tarot, fortune, waifu)]
struct General;

#[group]
#[commands(suggest, source)]
struct Meta;

#[group]
#[owners_only]
#[prefix = "do"]
#[commands(eload, letshang)]
struct Maintenance;

#[command]
async fn eload(ctx: &Context, msg: &Message, _args: Args) -> CommandResult {
    println!("Building emote cache...");
    let mut map = EMOTEMAP.write().await;
    map.clear();
    let emotes = PRIVSERVER.emojis(ctx).await.unwrap();
    for emote in emotes {
        let name = emote.name.clone();
        map.insert(name, emote);
    }
    let numemo = map.len();
    println!("Cache built, holding {} emotes", numemo);
    msg.channel_id
        .say(&ctx, format!("Cache built, holding {} emotes", numemo))
        .await?;
    Ok(())
}

#[command]
async fn letshang(ctx: &Context, msg: &Message, _args: Args) -> CommandResult {
    msg.channel_id
        .say(&ctx, "<:letshang:843870927354593390>")
        .await?;
    Ok(())
}

#[check]
#[name = "Indev"]
#[check_in_help(false)]
async fn indev_check(
    ctx: &Context,
    msg: &Message,
    _: &mut Args,
    _: &CommandOptions,
) -> Result<(), Reason> {
    if msg.author.id != 176380087736860672 {
        msg.channel_id
            .say(&ctx.http, "This feature is currently in development")
            .await
            .unwrap();
        return Err(Reason::UserAndLog {
            user: "Holo has not deemed you worthy".to_string(),
            log: "someone tried to enter a holo only command".to_string(),
        });
    }
    Ok(())
}

#[command]
#[description = "Show some info about Azura"]
#[num_args(0)]
async fn source(ctx: &Context, msg: &Message, _args: Args) -> CommandResult {
    msg.channel_id
        .send_message(&ctx, |m| {
            m.embed(|e| {
                e.title("Azura - A simple bot, written in rust")
                    .color(serenity::utils::Color::from_rgb(192, 255, 238))
                    .description(format!("Use `~help` for an overview of commands"))
                    .field("Version", env!("CARGO_PKG_VERSION"), false)
                    .field(
                        "Source",
                        "[Gitlab Repository](https://gitlab.com/Azurios/azura)",
                        false,
                    )
            })
        })
        .await?;
    Ok(())
}

#[command]
#[description = "Suggest a feature for Azura. Mind the double quotes around your arguments!"]
#[usage = "<title> <description>"]
#[example(" \"Cool feature\" \"With the ability to do more cool stuff or something\"")]
#[num_args(2)]
async fn suggest(ctx: &Context, msg: &Message, mut args: Args) -> CommandResult {
    msg.channel_id
        .say(
            &ctx.http,
            format!(
                "Got your suggestion, thank you! {}",
                commands::emote::get_emote("love").await.mention()
            ),
        )
        .await?;
    UserId::from(176380087736860672)
        .create_dm_channel(&ctx.http)
        .await?
        .say(ctx, msg.author.tag() + &msg.content)
        .await?;
    let quotetrim = |str: String| str.replace("\"", "");
    let title = quotetrim(args.single::<String>().unwrap());
    let mut text = quotetrim(args.single::<String>().unwrap());
    let aut = msg.author_nick(ctx).await.unwrap_or(msg.author.tag());
    text.push_str(&format!(
        "\n\n This issue was automatically created. \n\n Invoking user: {}",
        aut
    ));
    let gittoken = std::fs::read_to_string(".gittoken").expect("error reading gittoken");
    let gittoken = gittoken.trim();
    let client = Gitlab::new("gitlab.com", gittoken).expect("err in gitlab client creation");
    let endpoint = projects::issues::CreateIssue::builder()
        .project("Azurios/azura")
        .title(title)
        .description(text)
        .build()
        .unwrap();
    let _ = api::ignore(endpoint)
        .query(&client)
        .expect("err during issue creation");
    Ok(())
}
