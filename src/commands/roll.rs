use super::super::*;
use num_bigint::*;
use rand::*;
use serenity::client::Context;
use serenity::framework::standard::{macros::command, Args, CommandResult};
use serenity::model::prelude::Message;
use std::cmp::Ordering;

#[command]
#[description = "Rolls a dice with however many sides you want"]
#[usage = "<x>` where `<x> is an arbitrarily large Integer"]
#[aliases("r")]
#[sub_commands(multi, float)]
#[num_args(1)]
async fn roll(ctx: &Context, msg: &Message, mut args: Args) -> CommandResult {
    let sides: BigInt = match args.single::<BigInt>() {
        Ok(res) => res,
        Err(_) => {
            msg.channel_id
                .say(ctx, "I don't think that's going to work.")
                .await?;
            return Ok(());
        }
    };

    let say_content = match sides.cmp(&BigInt::from(0)) {
        Ordering::Equal => String::from("How many edges does a zero-sided die have?"),
        Ordering::Less => format!("I tried rolling a D-`{}`, but I failed :(", sides),
        Ordering::Greater => {
            let result: BigInt =
                rand::thread_rng().gen_bigint_range(&BigInt::from(1), &(sides + BigInt::from(1)));
            format!("You rolled a `{}`!", result)
        }
    };
    match msg.channel_id.say(&ctx.http, say_content).await {
        Err(serenity::Error::Model(ModelError::MessageTooLong(amnt))) => {
            msg.channel_id
                .say(
                    &ctx.http,
                    format!("My reply would be too long by {} characters!", { amnt }),
                )
                .await?;
        }
        Err(_) => {}
        Ok(_) => {}
    };

    Ok(())
}

#[command]
#[description = "Rolls multiple dice of the same kind. If there's trouble parsing, defaults do 1 die, or a d6"]
#[example = "2d6 4d7 1d100"]
#[help_available]
async fn multi(ctx: &Context, msg: &Message, mut args: Args) -> CommandResult {
    let mut messagecontent = "You rolled:\n".to_string();
    for arg in args.iter::<String>() {
        let arg = arg.unwrap_or("1d6".to_string());
        let mut v = arg.split('d').into_iter();
        let numdice = v.next().unwrap().parse::<u16>().unwrap_or(1);
        let sides = v.next().unwrap().parse::<u16>().unwrap_or(6);
        for x in 1..(numdice + 1) {
            let res = rand::thread_rng().gen_range(1..(sides + 1));
            messagecontent.push_str(&format!("On d{} number {} : `{}`\n", sides, x, res));
        }
    }
    // check if reply would be too long
    match msg.channel_id.say(&ctx.http, messagecontent).await {
        Err(serenity::Error::Model(ModelError::MessageTooLong(amnt))) => {
            msg.channel_id
                .say(
                    &ctx.http,
                    format!("My reply would be too long by {} characters!", { amnt }),
                )
                .await?;
        }
        // This is clearly not proper code
        _ => (),
    };
    Ok(())
}
#[command]
#[description = "Rolls a 'floating-point-sided' die"]
#[example = "3.141"]
#[num_args(1)]
#[help_available]
async fn float(ctx: &Context, msg: &Message, mut args: Args) -> CommandResult {
    let v: f32 = match args.single::<f32>() {
        Ok(val) => val,
        Err(_) => {
            msg.channel_id
                .say(ctx, "I don't think that's going to work.")
                .await?;
            return Ok(());
        }
    };

    let res = match v.partial_cmp(&0.0).unwrap_or(Ordering::Equal) {
        Ordering::Greater => format!("You rolled a `{}`", rand::thread_rng().gen_range(0.0..v)),
        _ => String::from("I don't think that's going to work"),
    };
    msg.channel_id.say(ctx, res).await?;
    Ok(())
}
