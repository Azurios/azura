use itertools::izip;
use serenity::client::Context;
use serenity::framework::standard::{macros::command, Args, CommandResult};
use serenity::model::prelude::Message;
const DESCR: [&str; 10] = [
    "Your condition at present",
    "Your obstacle and trouble at present",
    "The best possible outcome for you",
    "The cause to your present situation",
    "Your immediate past",
    "Your immediate future",
    "You at present",
    "Your surroundings at present",
    "Your hopes and fears",
    "The outcome",
];

#[derive(serde::Deserialize, Debug)]
struct Outer {
    nhits: i32,
    cards: Vec<Card>,
}

#[derive(serde::Deserialize, Debug)]
struct Card {
    name_short: String,
    name: String,
    value: String,
    value_int: i32,
    #[serde(alias = "type")]
    majmin: String,
    meaning_up: String,
    meaning_rev: String,
    desc: String,
}
#[command]
#[description = "A commandgroup related to Tarot-cards. Use the subcommands for specific actions."]
#[sub_commands(reading, search)]
async fn tarot(ctx: &Context, msg: &Message, _args: Args) -> CommandResult {
    msg.channel_id
        .say(&ctx.http, "You should call a subcommand")
        .await?;
    Ok(())
}

#[command]
#[description = "Returns a prediction of your future"]
#[usage = "<Question>` where `<Question> is a string enclosed by double quotes`"]
#[example = "\"How will I fare in the tournament?\""]
#[example = "\"Today\""]
#[num_args(1)]
async fn reading(ctx: &Context, msg: &Message, _args: Args) -> CommandResult {
    let response = reqwest::get("https://rws-cards-api.herokuapp.com/api/v1/cards/random?n=10")
        .await?
        .text()
        .await?;
    let outer: Outer = serde_json::from_str(&response).unwrap();
    let cards = outer.cards;
    let mut readings = Vec::new();
    let mut truth = Vec::new();
    for card in cards {
        let mut namemean = format!("**{}** \n {}", card.name, card.meaning_up);
        namemean.truncate(1024);
        readings.push(namemean);
        truth.push(true);
    }

    msg.channel_id
        .send_message(&ctx, |m| {
            m.embed(|e| {
                e.title("The future holds...");
                e.color(serenity::utils::Color::from_rgb(16, 73, 17));
                e.description(format!("I have had an audience with the stars about your question, {}. Here is what they say:", msg.author));
                e.fields(izip!(DESCR.iter(), readings.iter(), truth))
            })
        })
        .await?;
    Ok(())
}

#[command]
#[description = "Searches for a tarot card. Use the format <suit><value> for best results.\n <suit> = cu | wa | pe | sw | ar \n cups/wands/pentacles/swords/major arcana \n <value> = 2-10 | ac | pa | kn | qu | ki \n 2-10/ace/page/knight/queen/king"]
#[example = "cupa"]
#[example = "ar07"]
#[example = "wa02"]
#[num_args(1)]
async fn search(ctx: &Context, msg: &Message, mut args: Args) -> CommandResult {
    let response = reqwest::get(format!(
        "https://rws-cards-api.herokuapp.com/api/v1/cards/search?q={}",
        args.single::<String>().unwrap()
    ))
    .await?
    .text()
    .await?;
    let outer: Outer = serde_json::from_str(&response).unwrap();
    if outer.cards.is_empty() {
        msg.channel_id
            .say(ctx, "The search returned no results")
            .await?;
    }
    let search = outer.cards.first().unwrap();
    msg.channel_id
        .send_message(&ctx, |m| {
            m.embed(|e| {
                e.field("Name", &search.name, true)
                    .field("Short version", &search.name_short, true)
                    .field("Value", &search.value, true)
                    .field("Meaning", &search.meaning_up, false)
                    .field("Reversed Meaning", &search.meaning_rev, true)
            })
        })
        .await?;
    Ok(())
}
