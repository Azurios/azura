use serenity::client::Context;
use serenity::prelude::*;
use serenity::{
    framework::standard::{macros::command, Args, CommandResult},
    model::channel::Message,
};
use std::collections::BTreeMap;

lazy_static! {
    pub static ref EMOTEMAP: RwLock<BTreeMap<String, serenity::model::guild::Emoji>> = {
        let m: BTreeMap<String, serenity::model::guild::Emoji> = BTreeMap::new();
        RwLock::new(m)
    };
}

#[command]
#[description = "Show some emotion!"]
#[min_args(1)]
#[usage = "<emotion>`, for a full list of emotions `emote all"]
#[aliases("e")]
async fn emote(ctx: &Context, msg: &Message, mut args: Args) -> CommandResult {
    let map = EMOTEMAP.read().await;
    if !msg.is_private() {
        msg.delete(ctx).await?;
    }
    let emotename = args.single::<String>()?;
    if emotename == "all" {
        // create a vector for storing fields
        let mut staticvec = Vec::new();
        let mut animvec = Vec::new();
        staticvec.push(String::new());
        animvec.push(String::new());

        for emote in map.keys() {
            if map.get(emote).unwrap().animated {
                if animvec.last().unwrap().len()
                    + emoji_to_string(map.get(emote).unwrap()).len()
                    + emote.len()
                    + 3
                    < 1024
                {
                    animvec.last_mut().unwrap().push_str(&format!(
                        "{} {}\n",
                        &emoji_to_string(map.get(emote).unwrap()),
                        emote
                    ));
                } else {
                    animvec.push(String::new());
                    animvec.last_mut().unwrap().push_str(&format!(
                        "{} {}\n",
                        &emoji_to_string(map.get(emote).unwrap()),
                        emote
                    ));
                }
            } else {
                if staticvec.last().unwrap().len()
                    + emoji_to_string(map.get(emote).unwrap()).len()
                    + emote.len()
                    + 3
                    < 1024
                {
                    staticvec.last_mut().unwrap().push_str(&format!(
                        "{} {}\n",
                        &emoji_to_string(map.get(emote).unwrap()),
                        emote
                    ));
                } else {
                    staticvec.push(String::new());
                    staticvec.last_mut().unwrap().push_str(&format!(
                        "{} {}\n",
                        &emoji_to_string(map.get(emote).unwrap()),
                        emote
                    ));
                }
            }
        }

        let sentmsg = msg
            .channel_id
            .send_message(&ctx, |m| {
                m.embed(|e| {
                    e.title("My knowledge of emotes");
                    e.color(serenity::utils::Color::from_rgb(192, 255, 238));
                    e.description(format!("Hi, I'm responding to {}. \n Try out the emotes by using `~emote yay` or replace `yay` with an emote below.", msg.author));
                    for str in staticvec {
                        e.field("Static", str, true);
                    }
                    for str in animvec {
                        e.field("Animated", str, true);
                    }
                    e
                })
            })
            .await?;
        tokio::time::sleep(tokio::time::Duration::from_secs(30)).await;
        sentmsg.delete(ctx).await?;
    } else if !map.contains_key(&emotename) && args.is_empty() {
        msg.channel_id
            .say(&ctx.http, format!("I don't know the emote `{}`", emotename))
            .await?;
    } else {
        let mut replstr = String::new();
        if map.contains_key(&emotename) {
            replstr.push_str(&emoji_to_string(map.get(&emotename).unwrap()));
        }

        let iter = args.iter::<String>();
        let strings: Vec<String> = iter
            .map(|arg| map.get(&arg.unwrap()))
            .filter(|x| x.is_some())
            .map(|x| emoji_to_string(x.unwrap()))
            .collect();

        for st in strings {
            replstr.push_str(&st);
        }
        if let serenity::model::channel::MessageType::InlineReply = msg.kind {
            msg.referenced_message
                .as_ref()
                .unwrap()
                .reply(&ctx.http, replstr)
                .await?;
        } else {
            msg.channel_id.say(&ctx.http, replstr).await?;
        }
    }
    Ok(())
}

pub async fn get_emote(name: &str) -> serenity::model::guild::Emoji {
    let map = EMOTEMAP.read().await;
    map.get(name).unwrap().clone()
}

pub fn emoji_to_string(e: &serenity::model::guild::Emoji) -> String {
    format!(
        "<{}:{}:{}>",
        if e.animated { "a" } else { "" },
        e.name,
        e.id
    )
}

#[command]
#[description = "React with emotion!"]
#[num_args(1)]
#[usage = "<emotion>` replying to the message you want to react to. For a full list of emotions, see `~emote all"]
async fn react(ctx: &Context, msg: &Message, mut args: Args) -> CommandResult {
    let map = EMOTEMAP.read().await;
    if !msg.is_private() {
        msg.delete(ctx).await?;
    }
    let emotename = args.single::<String>()?;
    if !map.contains_key(&emotename) {
        msg.channel_id
            .say(&ctx.http, format!("I don't know the emote `{}`", emotename))
            .await?;
    } else {
        let reaction = get_emote(&emotename).await;
        if let serenity::model::channel::MessageType::InlineReply = msg.kind {
            msg.referenced_message
                .as_ref()
                .unwrap()
                .react(&ctx.http, reaction)
                .await?;
        } else {
            msg.channel_id
                .say(&ctx.http, "Cant find a message to react to".to_string())
                .await?;
        }
    }
    Ok(())
}
