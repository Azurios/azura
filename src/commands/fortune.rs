use serenity::client::Context;
use serenity::{
    framework::standard::{macros::command, Args, CommandResult},
    model::channel::Message,
};
use std::process::Command;
use std::str;

#[command]
// #[checks(indev)]
#[description = "Crack open a fortune cookie"]
#[aliases("fc")]
async fn fortune(ctx: &Context, msg: &Message, _args: Args) -> CommandResult {
    if !msg.is_private() {
        // in server, can delete
        msg.delete(ctx).await?;
    }
    let out = Command::new("fortune").output().unwrap().stdout.to_vec();
    let resstr = str::from_utf8(&out).unwrap();
    msg.channel_id.say(ctx, resstr).await?;
    Ok(())
}
