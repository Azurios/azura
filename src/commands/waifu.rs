use serenity::client::Context;
use serenity::futures::StreamExt;
use serenity::{
    framework::standard::{macros::command, Args, CommandResult},
    model::channel::Message,
};

#[command]
#[description = "Get a picture of your new dream waifu"]
#[aliases("uwu")]
async fn waifu(ctx: &Context, msg: &Message, mut args: Args) -> CommandResult {
    if !msg.is_private() {
        msg.delete(ctx).await?;
    }
    let mut sentmsg = msg
        .channel_id
        .send_message(&ctx, |m| {
            m.embed(|e| {
                e.title("Generate *your* perfect waifu");
                e.color(serenity::utils::Color::from_rgb(192, 255, 238));
                e.description(format!(
                    "Please be patient, {}, your waifu will arrive shortly...",
                    msg.author
                ));
                e
            })
        })
        .await?;

    let client = reqwest::Client::new();
    let mut new_waifu: WaifuRequest = Default::default();
    let request = client.post("https://api.waifulabs.com/generate");
    // println!("{}", serde_json::to_string(&new_waifu).unwrap());
    let request = request
        .body(serde_json::to_string(&new_waifu).unwrap())
        .build()
        .unwrap();
    let mut response = client.execute(request).await.unwrap().text().await.unwrap();
    let mut responsejson: Root = serde_json::from_str(&response).unwrap();

    let mut filelist: Vec<String> = Vec::new();
    for j in 0..16 {
        let b64 = base64::decode(&responsejson.new_girls.get(j).unwrap().image).unwrap();
        let img = image::load_from_memory(&b64).unwrap();
        img.save(format!("waifu-{}.png", j)).unwrap();
        filelist.push(format!("waifu-{}.png", j));
    }

    std::process::Command::new("magick")
        .arg("montage")
        .args(filelist)
        .arg("out.png")
        .spawn()
        .unwrap()
        .wait()
        .unwrap();

    sentmsg
        .edit(&ctx, |m| {
            m.embed(|e| {
                e.title("Generate *your* perfect waifu");
                e.color(serenity::utils::Color::from_rgb(192, 255, 238));
                e.description(format!("Here's a first selection!"));
                e
            })
        })
        .await?;

    let sentpic = msg
        .channel_id
        .send_message(&ctx, |m| {
            m.add_file("out.png");
            m
        })
        .await?;

    let mut coll = serenity::collector::message_collector::MessageCollectorBuilder::new(&ctx.shard)
        .timeout(std::time::Duration::new(60, 0))
        .channel_id(msg.channel_id)
        .author_id(msg.author.id)
        .await;
    let mut chosen = match coll.next().await {
        None => 0,
        Some(msg) => (msg.content).clone().parse().unwrap_or(0),
    };

    drop(coll);

    for i in 1..4 {
        if !(chosen > 15) {
            let selectedgirl = responsejson.new_girls.get(chosen).unwrap().seeds.to_owned();
            new_waifu = WaifuRequest {
                current_girl: selectedgirl,
                step: i,
            };
        } else {
            new_waifu.step = i;
        }
        let request = client.post("https://api.waifulabs.com/generate_big");
        // println!("{}", serde_json::to_string(&new_waifu).unwrap());
        let new_waifu_big = BigWaifu {
            current_girl: new_waifu.current_girl.clone(),
            step: i,
            size: 1024,
        };
        let request = request
            .body(serde_json::to_string(&new_waifu_big).unwrap())
            .build()
            .unwrap();
        let bigres = client.execute(request).await.unwrap().text().await.unwrap();
        //println!("{}", bigres);
        let bigresjson: BigRoot = serde_json::from_str(&bigres).unwrap();
        let b64 = base64::decode(&bigresjson.girl.to_string()).unwrap();
        let img = image::load_from_memory(&b64).unwrap();
        img.save(format!("preview.png")).unwrap();

        let sentpic = msg
            .channel_id
            .send_message(&ctx, |m| {
                m.add_file("preview.png");
                m
            })
            .await?;
        let request = client.post("https://api.waifulabs.com/generate");
        // println!("{}", serde_json::to_string(&new_waifu).unwrap());
        let request = request
            .body(serde_json::to_string(&new_waifu).unwrap())
            .build()
            .unwrap();
        response = client.execute(request).await.unwrap().text().await.unwrap();
        responsejson = serde_json::from_str(&response).unwrap();

        let mut filelist: Vec<String> = Vec::new();
        for j in 0..16 {
            let b64 = base64::decode(&responsejson.new_girls.get(j).unwrap().image).unwrap();
            let img = image::load_from_memory(&b64).unwrap();
            img.save(format!("waifu-{}.png", j)).unwrap();
            filelist.push(format!("waifu-{}.png", j));
        }

        std::process::Command::new("magick")
            .arg("montage")
            .args(filelist)
            .arg("out.png")
            .spawn()
            .unwrap()
            .wait()
            .unwrap();

        sentmsg
            .edit(&ctx, |m| {
                m.embed(|e| {
                    e.title("Generate *your* perfect waifu");
                    e.color(serenity::utils::Color::from_rgb(192, 255, 238));
                    e.description(format!("Here's a first selection!"));
                    e
                })
            })
            .await?;

        let sentpic = msg
            .channel_id
            .send_message(&ctx, |m| {
                m.add_file("out.png");
                m
            })
            .await?;

        let mut coll =
            serenity::collector::message_collector::MessageCollectorBuilder::new(&ctx.shard)
                .timeout(std::time::Duration::new(60, 0))
                .channel_id(msg.channel_id)
                .author_id(msg.author.id)
                .await;
        chosen = match coll.next().await {
            None => 16,
            Some(msg) => (msg.content).clone().parse().unwrap_or(16),
        };
        if i == 3 {
            if !(chosen > 15) {
                let selectedgirl = responsejson.new_girls.get(chosen).unwrap().seeds.to_owned();
                new_waifu = WaifuRequest {
                    current_girl: selectedgirl,
                    step: i,
                };
            } else {
                new_waifu.step = i;
            }
            let request = client.post("https://api.waifulabs.com/generate_big");
            // println!("{}", serde_json::to_string(&new_waifu).unwrap());
            let new_waifu_big = BigWaifu {
                current_girl: new_waifu.current_girl.clone(),
                step: i,
                size: 1024,
            };
            let request = request
                .body(serde_json::to_string(&new_waifu_big).unwrap())
                .build()
                .unwrap();
            let bigres = client.execute(request).await.unwrap().text().await.unwrap();
            //println!("{}", bigres);
            let bigresjson: BigRoot = serde_json::from_str(&bigres).unwrap();
            let b64 = base64::decode(&bigresjson.girl.to_string()).unwrap();
            let img = image::load_from_memory(&b64).unwrap();
            img.save(format!("preview.png")).unwrap();

            let sentpic = msg
                .channel_id
                .send_message(&ctx, |m| {
                    m.add_file("preview.png");
                    m
                })
                .await?;
        }
        drop(coll);
    }
    /*
    let mut userinput = String::new();
    std::io::stdin().read_line(&mut userinput).unwrap_or(0);
    let userchoice: usize = userinput.trim().parse().unwrap();
    let selectedgirl = responsejson
        .new_girls
        .get(userchoice)
        .unwrap()
        .seeds
        .to_owned();
    new_waifu = WaifuRequest {
        current_girl: selectedgirl,
        step: i,
    };
    */
    Ok(())
}

#[derive(Default, Debug, Clone, PartialEq, serde_derive::Serialize, serde_derive::Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Root {
    pub new_girls: Vec<Waifu>,
}

#[derive(Default, Debug, Clone, PartialEq, serde_derive::Serialize, serde_derive::Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct BigRoot {
    pub girl: String,
}

#[derive(Default, Debug, Clone, PartialEq, serde_derive::Serialize, serde_derive::Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Waifu {
    pub image: String,
    pub seeds: Vec<::serde_json::Value>,
}
#[derive(Default, Debug, Clone, PartialEq, serde_derive::Serialize, serde_derive::Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct BigWaifu {
    pub step: i32,
    pub current_girl: Vec<::serde_json::Value>,
    pub size: i32,
}

#[derive(Default, Debug, Clone, PartialEq, serde_derive::Serialize, serde_derive::Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct WaifuRequest {
    pub step: i32,
    pub current_girl: Vec<::serde_json::Value>,
}
